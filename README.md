# Summary
A beautified Tanuki Tech using .Net 3.1, bootstrap and deploying to Azure Kubernetes Service.

> .Net Core 5 still needs a buildpack from either Heroku or CloudFoundry. CloudFoundry seems to have one on the way.

## Setup (BASICS)

### Get rolling locally
1. Click 'fork' on the project page. This will duplicate my project with you as owner, allowing you to do whatever you want to it.
2. Hit the 'Clone' button on the new project page
2. Either select "git clone REPOSITORY_URL.git" from local machine or download the project as a zip and extract on local machine
> More info on ["git clone"](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh)
2. Within a terminal/powershell window, cd to the project folder
3. Run "dotnet watch run"
    * ***"watch"*** will allow you to access the app from http://localhost:5000 and auto-update the page when code is changed and saved within ide (no need to rebuild, redeploy locally, etc, for small changes)
> If dotnet command is unrecognized, [download the 'latest' SDK](https://dotnet.microsoft.com/download) and then retry the command.
4. Tinker with the app locally via localhost
5. When ready, save all changes and do a git commit and git push

> If these are unfamiliar concepts, either watch the video and use VSCode like me, or download GitKraken - the best Git GUI tool that I have seen for learning Git

### Git tips
In this scenario, you should only have to do the following commands to push your changes from your local machine to Gitlab.
Make sure you are in the top level of the project's directory prior to running these commands.
> Pro tip: There should be a hidden .git folder in the directory where you run these commands
```
git add .
git commit - "YOU COMMIT MESSAGE"
git push
```
## Setup (ADVANCED)
> TODO: documentation

### Azure Kubernetes Service
 * Create cluster
 * Cluster settings
 * Get cluster info

### Project settings 
 * Auto-devops
 * Cluster configuration
 * Test run
